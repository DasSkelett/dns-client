# DNS Client

A simple command line utility written in Go that supports current and in-draft resource record types (e.g. [SVCB and HTTPS](https://datatracker.ietf.org/doc/draft-ietf-dnsop-svcb-https/)).

Uses [github.com/miekg/dns](https://github.com/miekg/dns) (BSD-3-Clause)

## Usage

Specify one or more domains to resolve as arguments.
Specify one or more resource record types to query as arguments prefixed with `+`.
Specify one or more DNS servers to query as arguments prefixed with `@`.

**Example:**

```sh
dns-client gitlab.com +AAAA +A @2620:fe::fe @2606:4700:4700::1111
```

Response:
```
;; opcode: QUERY, status: NOERROR, id: 57165
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;gitlab.com.    IN       A

;; ANSWER SECTION:
gitlab.com.     203     IN      A       172.65.251.78

Server: [2606:4700:4700::1111]:53 - RTT: 24.918596ms - Error: <nil>
;; opcode: QUERY, status: NOERROR, id: 24854
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;gitlab.com.    IN       AAAA

;; ANSWER SECTION:
gitlab.com.     202     IN      AAAA    2606:4700:90:0:f22e:fbec:5bed:a9b9

Server: [2606:4700:4700::1111]:53 - RTT: 25.037656ms - Error: <nil>
;; opcode: QUERY, status: NOERROR, id: 57165
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;gitlab.com.    IN       A

;; ANSWER SECTION:
gitlab.com.     173     IN      A       172.65.251.78

Server: [2620:fe::fe]:53 - RTT: 33.84814ms - Error: <nil>
;; opcode: QUERY, status: NOERROR, id: 24854
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;gitlab.com.    IN       AAAA

;; ANSWER SECTION:
gitlab.com.     64      IN      AAAA    2606:4700:90:0:f22e:fbec:5bed:a9b9

Server: [2620:fe::fe]:53 - RTT: 42.370727ms - Error: <nil>
```
