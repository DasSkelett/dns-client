package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"sync"

	"github.com/miekg/dns"
)

func main() {
	args := os.Args

	var domains []string
	var types []uint16
	var servers []string

	for _, a := range args[1:] {
		if strings.HasPrefix(a, "+") {
			types = append(types, dns.StringToType[strings.TrimPrefix(a, "+")])
		} else if strings.HasPrefix(a, "@") {
			s := net.JoinHostPort(strings.TrimLeft(a, "@"), "53")
			servers = append(servers, s)
		} else {
			domains = append(domains, dns.CanonicalName(a))
		}
	}

	var messages []*dns.Msg

	for _, d := range domains {
		for _, t := range types {
			m := new(dns.Msg)
			m.SetQuestion(d, t)
			messages = append(messages, m)
		}
	}

	c := new(dns.Client)
	wg := new(sync.WaitGroup)

	for _, m := range messages {
		for _, s := range servers {
			wg.Add(1)
			go func(m *dns.Msg, s string) {
				in, rtt, err := c.Exchange(m, s)
				fmt.Printf("%v\nServer: %v - RTT: %v - Error: %v\n", in, s, rtt, err)
				wg.Done()
			}(m.Copy(), s)
		}
	}

	wg.Wait()
}
